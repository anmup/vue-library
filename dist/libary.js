'use strict';

var Vue = require('vue');
var Vuex = require('vuex');
var VueCookies = require('vue-cookies');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var Vue__default = /*#__PURE__*/_interopDefaultLegacy(Vue);
var Vuex__default = /*#__PURE__*/_interopDefaultLegacy(Vuex);
var VueCookies__default = /*#__PURE__*/_interopDefaultLegacy(VueCookies);

var script$1 = {
    name: 'InputText'
};

function render$1(_ctx, _cache, $props, $setup, $data, $options) {
  return (Vue.openBlock(), Vue.createElementBlock("input"))
}

script$1.render = render$1;
script$1.__file = "src/components/InputText.vue";

var script = {
    name: 'InputTextArea'
};

function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (Vue.openBlock(), Vue.createElementBlock("textarea"))
}

script.render = render;
script.__file = "src/components/InputTextArea.vue";

var components = {
    InputText: script$1,
    InputTextArea: script
};

Vue__default["default"].use(Vuex__default["default"]);
//Vue.use(axios);
Vue__default["default"].use(VueCookies__default["default"]);

const store = new Vuex__default["default"].Store({
    state: {
        baseURL: "//" + window.location.hostname + "/",
        currentUser: {
            access_token: undefined,
            id: -1,
        },
        auth: false,
    },
    getters: {
        getCurrentUser(state) {
            return state.currentUser;
        },
        isAuth(state) {
            return state.auth;
        },
        baseURL(state) {
            return state.baseURL;
        }
    },
    mutations: {
        setCurrentUser(state, payload) {
            VueCookies__default["default"].set("user", payload);
            state.currentUser = payload;
        },

        setAuth(state, payload) {
            state.auth = payload;
        },
        setUrl(state, payload) {
            state.baseURL = payload;
        }
    },
    actions: {
        setURL({
            commit
        }, data) {
            return new Promise((resolve, reject) => {
                commit('setUrl', data);
                resolve(this.getters.isAuth);

            })
        },
        updateIsAuth({
            commit
        }, data) {
            return new Promise((resolve, reject) => {
                commit('setAuth', data);
                resolve(this.getters.isAuth);

            })
        },
        Login({
            commit
        }, data) {
            return new Promise((resolve, reject) => {
                axios.post(this.getters.baseURL + data.url, data)
                    .then((res) => {
                        if (res.data.auth) {
                            axios.defaults.headers.common['Authorization'] = "Bearer " + res.data.user.access_token;
                            commit('setCurrentUser', res.data.user);
                            commit('setAuth', true);
                        }
                        resolve(res);
                    }).catch((err) => {
                        reject(err);
                    });
            })
        },
        setTokenForRequest({
            commit
        }, data) {
            return new Promise((resolve, reject) => {
                let oldAuth = VueCookies__default["default"].get("user");
                if (oldAuth && oldAuth.access_token) {
                    axios.defaults.headers.common['Authorization'] = "Bearer " + oldAuth.access_token;
                }
                axios.get(this.getters.baseURL + data.url)
                    .then(res => {
                        let authServer = res.data.auth;
                        commit('setAuth', authServer);
                        if (authServer) {
                            commit('setCurrentUser', res.data.user);
                            axios.defaults.headers.common['Authorization'] = "Bearer " + res.data.user.access_token;
                        }
                        resolve(this.getters.isAuth);
                    }).catch(err => {
                        commit('setAuth', false);
                        reject(err);
                    });


            });
        },
        Logout({
            commit
        }, data) {
            return new Promise((resolve, reject) => {
                axios.post(this.getters.baseURL + data.url, data.data)
                    .then((res) => {
                        commit('setCurrentUser', res.data.user);
                        $cookies.remove("access_token");
                        commit('setAuth', false);
                        resolve(this.getters.isAuth);
                    }).catch((err) => {
                        reject(err);
                    });
            })
        },
    },
});

var auth = {
    install(Vue, defaultOptions) {
        Vue.prototype.$authTradein = this;
    },
    login: function (data) {
        return store.dispatch('Login', data).then(res => {
            return Promise.resolve(res);
        }).catch(err => {
            return Promise.reject(err);
        });
    },
    checkToken: function (url) {
        return store.dispatch('setTokenForRequest', {
            url: url
        }).then(res => {
            return Promise.resolve(res);
        }).catch(err => {
            return Promise.reject(err);
        })
    },
    logout: function (url, data) {
        return store.dispatch('Logout', {
            url: url,
            data: data
        }).then(res => {
            return Promise.resolve(res);
        }).catch(err => {
            return Promise.reject(err);
        });
    },
    setURL: function (url) {
        store.dispatch('setURL', url);
    },
    isAuth: function () {
        return store.getters.isAuth;
    }
};

const ui = {
    install (Vue) {
        for (const prop in components) {
            if(components.hasOwnProperty(prop)) {
                const component = components[prop];
                Vue.component(component.name, component);
            }
        }
    }
};

var index = {
    ui,
    auth
};

module.exports = index;
