import { defineAsyncComponent } from 'vue'

export const pagesComponents = {
  // path: /
  "v-8daa1a0e": defineAsyncComponent(() => import(/* webpackChunkName: "v-8daa1a0e" */"/Users/balkasti/Documents/AO/vue-library/docs/.vuepress/.temp/pages/index.html.vue")),
  // path: /components/ao-button.html
  "v-040ee908": defineAsyncComponent(() => import(/* webpackChunkName: "v-040ee908" */"/Users/balkasti/Documents/AO/vue-library/docs/.vuepress/.temp/pages/components/ao-button.html.vue")),
  // path: /components/auth.html
  "v-b35f661e": defineAsyncComponent(() => import(/* webpackChunkName: "v-b35f661e" */"/Users/balkasti/Documents/AO/vue-library/docs/.vuepress/.temp/pages/components/auth.html.vue")),
  // path: /components/input-text-area.html
  "v-2723036f": defineAsyncComponent(() => import(/* webpackChunkName: "v-2723036f" */"/Users/balkasti/Documents/AO/vue-library/docs/.vuepress/.temp/pages/components/input-text-area.html.vue")),
  // path: /components/input-text.html
  "v-eb1c736e": defineAsyncComponent(() => import(/* webpackChunkName: "v-eb1c736e" */"/Users/balkasti/Documents/AO/vue-library/docs/.vuepress/.temp/pages/components/input-text.html.vue")),
  // path: /404.html
  "v-3706649a": defineAsyncComponent(() => import(/* webpackChunkName: "v-3706649a" */"/Users/balkasti/Documents/AO/vue-library/docs/.vuepress/.temp/pages/404.html.vue")),
}
