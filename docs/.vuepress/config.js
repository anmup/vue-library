import { path } from '@vuepress/utils';
import { defaultTheme } from 'vuepress'
// import { getDirname, path } from '@vuepress/utils'
import { registerComponentsPlugin } from '@vuepress/plugin-register-components'

// const __dirname = getDirname(import.meta.url)

export default {
    plugins: [
        registerComponentsPlugin({
            componentsDir: path.resolve(__dirname, '../../src/components'),
        }),
    ],
    theme: defaultTheme({
        // default theme config
        navbar: [
          {
            text: 'Home',
            link: '/',
          },
          {
              text: 'Anmup',
              link: 'https://www.anmup.online'
          }
        ],
        sidebar: [
            {
                title: "Components",
                collapsable: true,
                children: [
                    '/ReadMe.md',
                    '/components/auth.md',
                    '/components/input-text.md',
                    '/components/input-text-area.md',
                    '/components/ao-button.md'
                ]
            }
        ]
      }),
}