import InputText from './components/InputText.vue'
import InputTextArea from './components/InputTextArea.vue'
import AoButton from './components/AoButton.vue'

export default {
    InputText,
    InputTextArea,
    AoButton
}