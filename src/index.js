import components from "./component-register";

const ui = {
    install (Vue) {
        for (const prop in components) {
            if(components.hasOwnProperty(prop)) {
                const component = components[prop]
                Vue.component(component.name, component)
            }
        }
    }
}

import auth from './plugins/auth'

export default {
    ui,
    auth
}